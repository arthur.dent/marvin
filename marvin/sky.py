import time
import ephem
from datetime import datetime, timedelta
from marvin import config, utils


class IssTracker:
    def __init__(self, marvin, site):
        self.marvin = marvin
        self.site = site
        self.iss_tle = utils.get_iss_tle()
        self.iss = self.find_iss(self.iss_tle)

    def _should_update(self, last_update):
        time_since_update = (datetime.utcnow() - last_update).total_seconds()
        return time_since_update > (20 * 60)

    @property
    def next_pass(self):
        tr, azr, tt, altt, ts, azs = self.site.next_pass(self.iss)

        if (ts > tr):
            duration = int((ts - tr) * 60 * 60 * 24)

        print("Next Pass (Localtime): %s" % ephem.localtime(tr))

        if config.INFO:
            print("UTC Rise Time   : %s" % tr)
            print("UTC Max Alt Time: %s" % tt)
            print("UTC Set Time    : %s" % ts)
            print("Rise Azimuth: %s" % azr)
            print("Set Azimuth : %s" % azs)
            print("Max Altitude: %s" % altt)
            print("Duration    : %s" % duration)

    def find_iss(self, iss_tle):
        iss = ephem.readtle(iss_tle[0], iss_tle[1], iss_tle[2])
        self.site.date = datetime.utcnow()
        iss.compute(self.site)
        return iss

    def simulate(self, minutes):
        """Simulates the next few minutes of the ISS trajectory"""
        self.marvin.turn_led_on("green")
        azOld = 0

        for i in range(0, minutes, config.SIMULATION_SPEED):
            self.site.date = datetime.utcnow() + timedelta(minutes=i)
            self.iss.compute(self.site)

            azDeg = utils.to_deg(self.iss.az)
            steps = int(round((azDeg - azOld) * config.STEPS_PER_DEGREE,0))
            if steps > 1024:
                steps = steps - 2048
            if steps < -1024:
                steps = steps + 2048
            azOld = azDeg
            self.marvin.move_stepper(steps)

            altDeg = int(round(utils.to_deg(self.iss.alt),0))
            self.marvin.move_servo("right", altDeg)
#        self.marvin.reset()

    def follow_iss(self):
        self.marvin.turn_led_on()
        azOld = 0
        altOld = 0
        last_update = datetime.utcnow()
        while True:
            # Get TLE Info only every 20 minutes
            if self._should_update(last_update):
                self.iss_tle = utils.get_iss_tle()
                last_update = datetime.utcnow()
            iss = self.find_iss(self.iss_tle)
            altDeg = int(round(utils.to_deg(iss.alt),0))
            if config.XRAY_VISION or altDeg > int(config.HOR):
                if config.INFO:
                    if altDeg > int(45):
                        print("ISS IS OVERHEAD")
                    elif altDeg < int(config.HOR):
                        print("ISS IS BELOW HORIZON")
                    else:
                        print("ISS IS VISIBLE")
                        print(altDeg)

                azDeg = utils.to_deg(iss.az)
                steps = int(round((azDeg - azOld) * config.STEPS_PER_DEGREE,0))
                if steps > 1024:
                    steps = steps - 2048
                if steps < -1024:
                    steps = steps + 2048
                azOld = azDeg
                self.marvin.move_stepper(steps)
                if altDeg != altOld:
                    self.marvin.move_servo("right", altDeg)
                    altOld = altDeg
                time.sleep(5)
            else:
                if config.INFO:
                    print("ISS IS BELOW HORIZON")
                self.marvin.reset()
                time.sleep(60)


class Pointer:
    def __init__(self, marvin, site):
        self.marvin = marvin
        self.site = site

    def point_to(self, body_to_point):
        self.marvin.turn_led_on("green")
        should_point = True
        azOld = 0
        while should_point:
            if body_to_point == "q":
                break
            elif not hasattr(ephem, body_to_point):
                print("Invalid body")
                body_to_point = input("Choose another celestial body or 'q' to quit: ")
            else:
                s = getattr(ephem, body_to_point)()
                s.compute(self.site)
                azDeg = utils.to_deg(s.az)
                steps = int(round((azDeg - azOld) * config.STEPS_PER_DEGREE,0))
                if steps > 1024:
                    steps = steps - 2048
                if steps < -1024:
                    steps = steps + 2048
                azOld = azDeg
                self.marvin.move_stepper(steps)
                self.marvin.move_servo("right", int(round(utils.to_deg(s.alt),0)))

                body_to_point = input("Choose another celestial body or 'q' to quit: ")

