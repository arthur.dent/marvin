# Marvin Robot - The ISS Tracker

![marvin-robot](https://raw.githubusercontent.com/cacarrara/marvin/master/static/marvin.png)

This work is being done gracefully at [Base12](http://base12.com.br)

## Installing
```bash
git clone https://gitlab.com/arthur.dent/marvin.git
cd marvin
make install
```


## Reference
This work is based on amazing job of [@rgrokett](https://github.com/rgrokett) available at
[ESP8266_ISSPointer](https://github.com/rgrokett/ESP8266_ISSPointer)  
Special thanks to [@Bill Shupp](https://wheretheiss.at/w/about) for development of the [API](https://wheretheiss.at/w/developer) with information about ISS location
